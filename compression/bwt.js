/**
 * Created by Vasiliy on 01.04.2015.
 */

function bwt(input) {
    var rotations = [], rotation;
    var i;
    var begin, end;
    var result = '';

    for (i = 0; i < input.length; i++) {
        end = input.slice(0, input.length - i);
        begin = input.slice(end.length, input.length);

        rotations.push(begin + end);
    }

    rotations.sort();

    for (i = 0; i < rotations.length; i++) {
        rotation = rotations[i];
        result += rotation[rotation.length - 1];
    }

    return result;
}

function inverseBwt(input) {
    var rotations = generateTable(input.length);
    var i, j;
    var split;

    for (i = input.length - 1; i >= 0; i--) {
        split = input.split('');

        for (j = 0; j < input.length; j++) {
            rotations[j][i] = split[j];
        }

        rotations.sort();
    }

    return getResult(rotations);
}

function generateTable(size) {
    var rotations = [];
    var i, j;

    for (i = 0; i < size; i++) {
        rotations[i] = [];
        for (j = 0; j < size; j++) {
            rotations[i][j] = '';
        }
    }

    return rotations;
}

function getResult(rotations) {
    var index;
    var i;

    for (i = 0; i < rotations.length; i++) {
        if (rotations[i][rotations.length - 1] === END_SYMBOL) {
            index = i;
        }
    }

    return rotations[index].join('');
}

const END_SYMBOL = '#';
var bwtTransformed = bwt('SOUNDCLOUD' + END_SYMBOL);
console.log(bwtTransformed);
console.log(inverseBwt(bwtTransformed));