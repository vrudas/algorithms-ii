/**
 * Created by Vasiliy on 23.02.2015.
 */

const SPECIAL_SYMBOL = '#';
const MIN_LENGTH = 3;

/**
 * Compress input string using RLE algorithm
 * @param {string} input - string to compress
 * @param {boolean} optimised - flag to use optimised compression
 * @return {string} output - compressed string
 */
function rleCompress(input, optimised) {
    var output = '';
    var counter = 0,
        symbol = input[0];

    if (input.length <= MIN_LENGTH) {
        return input;
    }

    for (var i = 0; i <= input.length; i++) {
        if (input[i] === symbol) {
            counter++;
        } else {
            if (optimised) {
                if (counter >= MIN_LENGTH) {
                    output += counter + symbol + SPECIAL_SYMBOL;
                } else {
                    output += symbol;
                }
            } else {
                output += counter + symbol + SPECIAL_SYMBOL;
            }
            symbol = input[i];
            counter = 1;
        }
    }

    return output;
}

/**
 * DeCompress compressed with RLE input string
 * @param {string} input - string to decompress
 * @return {string} output - decompressed string
 */
function rleDecompress(input) {
    if (input.length <= MIN_LENGTH && !containsString(input, SPECIAL_SYMBOL)) {
        return input;
    }

    var pairs = input.split(SPECIAL_SYMBOL);
    var repeatCounter, repeatSymbol, output = '';

    for (var i in pairs) {
        repeatCounter = pairs[i].split(/\D/).shift();
        repeatSymbol = pairs[i].split(/\d/).pop();

        for (var j = 0; j < repeatCounter; j++) {
            output += repeatSymbol;
        }
    }

    return output;
}

/**
 * Check is input contains a string
 * @param input - string for checking
 * @param containString - string for contains check
 * @return {boolean} - result of containing
 */
function containsString(input, containString) {
    return input.indexOf(containString) > -1;
}

function main() {
    var inputText = [
        'A',
        'AA',
        'AAA',
        'AAAA',
        'AAAAA',
        'AAAAAB',
        'BAAAAA',
        'BAAAAB'
    ];

    for (var i in inputText) {
        console.log(rleCompress(inputText[i], true));
        console.log(rleDecompress(rleCompress(inputText[i], false)));
    }
}

main();