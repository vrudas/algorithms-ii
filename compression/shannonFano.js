/**
 * Created by Vasiliy on 25.02.2015.
 */
/**
 * Storage of symbol properties for decoding
 */
var _symbols;

/**
 * Represents object of SymbolProperties
 * @constructor
 */
function SymbolProperties() {
    this.symbol = '';
    this.count = 0;
    this.probability = 0;
    this.code = '';
}

/**
 * Compress string using Shannon-Fano Algorithm
 * @param {string} input - string to compress
 * @return {string} encoded input string
 */
function shannonFanoCompressing(input) {
    var counts = countSymbols(input);
    var length = input.length;

    var symbols = getSymbolsWithProperties(counts, length);

    shannonFanoSplit(symbols, 0, symbols.length - 1);

    console.log(symbols);

    _symbols = symbols;

    return compressString(input, symbols);
}

/**
 * Shannon-Fano splitting symbols for getting codes
 * @param {Array<SymbolProperties>} symbols - to split
 * @param {number} start - position
 * @param {number} end - position
 */
function shannonFanoSplit(symbols, start, end) {
    var mid,
        size = end - start + 1;
    if (size > 1) {
        mid = parseInt(size / 2 + start);
        for (var i = start; i <= end; i++) {
            if (i < mid) {
                symbols[i].code += '0';
            } else {
                symbols[i].code += '1';
            }
        }
        shannonFanoSplit(symbols, start, mid - 1);
        shannonFanoSplit(symbols, mid, end);
    }
}

/**
 * Counts each symbol in string
 * @param {string} input - string to compress
 * @return {Array} pair of type: (symbol, count)
 */
function countSymbols(input) {
    var counts = [];

    for (var i = 0; i < input.length; i++) {
        if (input[i] in counts) {
            counts[input[i]]++;
        } else {
            counts[input[i]] = 1;
        }
    }

    return counts;
}

/**
 * Generates properties for each symbol in {counts}
 * @param {Array} counts - pair of type: (symbol, count)
 * @param {number} inputLength of string to compress
 * @return {Array.<SymbolProperties>}
 */
function getSymbolsWithProperties(counts, inputLength) {
    var symbols = [];

    for (var symbol in counts) {
        var symbolProperties = new SymbolProperties();

        symbolProperties.symbol = symbol;
        symbolProperties.count = counts[symbol];
        symbolProperties.probability = symbolProperties.count / inputLength;

        symbols.push(symbolProperties);
    }

    return symbols.sort(compareProperty).reverse();
}

/**
 * Compare function for SymbolPropertiesObject
 * @param {SymbolProperties} firstSymbol to compare
 * @param {SymbolProperties} secondSymbol to compare
 * @return {number} compare result
 */
function compareProperty(firstSymbol, secondSymbol) {
    return firstSymbol.probability - secondSymbol.probability;
}

/**
 * Compress string into bytes
 * @param {string} uncompressed string to compress
 * @param {Array<SymbolProperties>} symbols
 * @return {string} encoded string
 */
function compressString(uncompressed, symbols) {
    var compressed = uncompressed;

    for (var i in symbols) {
        var regexp = new RegExp(symbols[i].symbol, 'g');
        compressed = compressed.replace(regexp, symbols[i].code);
    }

    return compressed;
}

/**
 * Decompress string from bytes
 * @param {string} input - string of encoded bytes
 * @param {Array<SymbolProperties>} symbols
 * @return {string} decoded string
 */
function decompressString(input, symbols) {
    var output = '';

    while (input.length !== 0) {
        for(var i in symbols){
            if(input.indexOf(symbols[i].code) === 0){
                input = input.substring(symbols[i].code.length, input.length);
                output += symbols[i].symbol;
            }
        }
    }

    return output;
}

function main() {
    var string = 'AAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBCCCCCCCCCCDDDDDDDDEEEEEEEE';

    var compressedString = shannonFanoCompressing(string);
    console.log(compressedString);

    var decompressedString = decompressString(compressedString, _symbols);
    console.log(decompressedString);

    console.log(decompressedString === string);
}

main();