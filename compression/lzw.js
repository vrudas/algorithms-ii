/**
 * Created by Vasiliy on 30.03.2015.
 */

const DICTIONARY_SIZE = 256;

function compress(uncompressed) {
    var dictionary = generateDictionary(true);

    var compressed = [];

    var word = '';
    var wordCurrent;
    var character;
    var dictionarySize = DICTIONARY_SIZE;

    for (var i = 0; i < uncompressed.length; i++) {
        character = uncompressed.charAt(i);
        wordCurrent = word + character;
        if (dictionary[wordCurrent]) {
            word = wordCurrent;
        } else {
            compressed.push(dictionary[word]);
            dictionary[wordCurrent] = dictionarySize++;
            word = String(character);
        }
    }

    if (word !== '') {
        compressed.push(dictionary[word]);
    }

    return compressed;
}

function decompress(compressed) {
    var dictionary = generateDictionary(false);
    var word;
    var decompressed;
    var dictSize = DICTIONARY_SIZE;
    var entry = '';

    word = String.fromCharCode(compressed[0]);
    decompressed = word;
    for (var i = 1; i < compressed.length; i += 1) {
        var character = compressed[i];
        if (dictionary[character]) {
            entry = dictionary[character];
        } else {
            if (character === dictSize) {
                entry = word + word.charAt(0);
            } else {
                return null;
            }
        }

        decompressed += entry;

        dictionary[dictSize++] = word + entry.charAt(0);

        word = entry;
    }

    return decompressed;
}

function generateDictionary(forCompress) {
    var dictionary;
    var i;

    if (forCompress) {
        dictionary = {};
        for (i = 0; i < DICTIONARY_SIZE; i++) {
            dictionary[String.fromCharCode(i)] = i;
        }
    } else {
        dictionary = [];
        for (i = 0; i < DICTIONARY_SIZE; i++) {
            dictionary[i] = String.fromCharCode(i);
        }
    }

    return dictionary;
}

function main() {
    var uncompressed = 'TOBEORNOTTOBEORTOBEORNOT';

    var compressed = compress(uncompressed);

    console.log(compressed);

    var decompressed = decompress(compressed);

    console.log(decompressed);

    console.log(decompressed === uncompressed);

}

main();