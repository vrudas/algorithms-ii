/**
 * Created by Vasiliy on 18.12.2014.
 */
function KnuthMorrisPrattMatcher(T, P) {
    var n, m, q, i, PI = [];

    n = T.length;
    m = P.length;

    computePrefixFunction(P, PI);
    printPrefixFunction(P, PI);
    q = 0;
    for (i = 0; i < n; i++) {
        while (q > 0 && P[q] != T[i])
            q = PI[q];
        if (P[q] == T[i])
            q++;
        if (q == m) {
            console.log("Pattern found with shift:", i - m);
            q = PI[q];
        }
    }
}

function computePrefixFunction(P, PI) {
    var m, q, k;

    m = P.length;
    PI[1] = 0;
    k = 0;
    for (q = 2; q <= m; q++) {
        while (k > 0 && P[k] !== P[q - 1]) {
            k = PI[k];
        }
        if (P[k] === P[q - 1]) {
            k++
        }
        PI[q] = k;
    }
}

function printPrefixFunction(P, PI) {
    var i, m;

    m = P.length;
    console.log("Prefix function PI:\n");
    for (i = 1; i <= m; i++)
        process.stdout.write("\t" + i);
    console.log();

    for (i = 1; i <= m; i++)
        process.stdout.write("\t" + P[i - 1]);
    console.log();

    for (i = 1; i <= m; i++)
        process.stdout.write("\t" + PI[i]);
    console.log();
}

KnuthMorrisPrattMatcher("bacbababaabcbababacac", "ababaca");