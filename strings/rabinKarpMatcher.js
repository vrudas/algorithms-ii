/**
 * Created by Vasiliy on 12/18/2014.
 */
function rabinKarpMatcher(T, P, d, q) {
    var n, m, h, p, t, s, i, j;

    n = T.length;
    m = P.length;

    //d^(m-1) mod q
    h = 1;
    for (i = 1; i <= m - 1; i++) {
        h = h * d % q;
    }

    p = 0;
    t = 0;

    // Prepare process
    for (i = 0; i < m; i++) {
        p = (d * p + ord(P[i]) - ord('0')) % q;
        t = (d * t + ord(T[i]) - ord('0')) % q;
    }

    for (s = 0; s <= n - m; s++) {
        if (p === t) {
            j = 0;
            while (j < m && P[j] === T[s + j]) {
                j++;
            }
            if (j === m) {
                console.log("Pattern found with shift:" + s);
            }
        }
        if (s < n - m) {
            t = (d * (t - (ord(T[s]) - ord('0')) * h) + ord(T[s + m]) - ord('0')) % q;
            if (t < 0) {
                t += q;
            }
        }
    }
}

function ord(c) {
    return c.charCodeAt(0);
}

var D = 10;// d-ary alphabet {0, 1, 2, ..., d-1}
var Q = 13;// modulus (dq just fits within one computer word)
rabinKarpMatcher("2359023141526739921", "31415", D, Q);