/**
 * Created by Vasiliy on 12/18/2014.
 */

/**
 * Naive String Matcher implemented by algorithm from KLRS book.
 * @type {{match: Function}}
 */
var NaiveStringMatcher = {
    /**
     * String match algorithm
     * @param t - template in which would be search
     * @param p - pattern for searching
     */
    match: function (t, p) {
        var s, i;
        var n = t.length;
        var m = p.length;

        for (s = 0; s <= n - m; s++) {
            i = 0;
            while (i < m && p[i] === t[s + i]) {
                i++;
            }
            if (i === m) {
                console.log('Pattern found with shift: ' + s);
            }
        }
    }
};

NaiveStringMatcher.match('acaabc', 'aab');