/**
 * Created by Vasiliy on 9/10/2014.
 */
var colors = {WHITE: 'WHITE', GRAY: 'GRAY', BLACK: 'BLACK'};
var values = {INFINITY: Number.INFINITY, NIL: null};

function Graph(v) {
    if (v !== undefined) {
        this.vertices = v;
    } else {
        this.vertices = 8;
    }

    this.edges = 0;
    this.time = 0;
    this.adj = [];
    this.color = [];
    this.d = [];
    this.f = [];
    this.pi = [];

    this.addEdge = addEdge;
    this.showGraph = showGraph;
    this.bfs = bfs;
    this.path = path;
    this.dfs = dfs;

    for (var i = 0; i < this.vertices; i++) {
        this.adj[i] = [];
        for (var j = 0; j < this.vertices; j++) {
            this.adj[i][j] = 0;
        }
        this.color[i] = colors.WHITE;
        this.d[i] = values.INFINITY;
        this.pi[i] = values.NIL;
    }
}

function addEdge(u, v) {
    this.adj[u][v] = 1;
    this.adj[v][u] = 1;
    this.edges++;
}

function buildGraph() {
    g.addEdge(0, 1);
    g.addEdge(0, 4);
    g.addEdge(1, 5);
    g.addEdge(2, 3);
    g.addEdge(2, 5);
    g.addEdge(2, 6);
    g.addEdge(3, 6);
    g.addEdge(3, 7);
    g.addEdge(5, 6);
    g.addEdge(6, 7);
}

function showGraph() {
    buildGraph();

    var graphString = '';
    for (var i = 0; i < this.vertices; i++) {
        for (var j = 0; j < this.vertices; j++) {
            if (this.adj[i][j] != undefined) {
                graphString += this.adj[i][j] + '\t';
            }
        }
        graphString += '\n';
    }
    console.log(graphString);
    console.log(" r --- s     t --- u      0 --- 1     2 --- 3  ");
    console.log(" |     |   / |   / |      |     |   / |   / |  ");
    console.log(" |     |  /  |  /  |      |     |  /  |  /  |  ");
    console.log(" |     | /   | /   |      |     | /   | /   |  ");
    console.log(" v     w --- x --- y      4     5 --- 6 --- 7  ");
}

function bfs(s) {
    this.color[s] = colors.GRAY;
    this.d[s] = 0;
    this.pi[s] = values.NIL;
    var queue = [];
    queue.push(s);

    while (queue.length != 0) {
        var u = queue.shift();
        for (var v = 0; v < this.vertices; v++) {
//            var v = this.adj[u][i];
            if (this.adj[u][v] == 1) {
                if (this.color[v] == colors.WHITE) {
                    this.color[v] = colors.GRAY;
                    this.d[v] = this.d[u] + 1;
                    this.pi[v] = u;
                    queue.push(v);
                }
            }
        }
        this.color[u] = colors.BLACK;
    }
}

function dfs() {
    this.time = 0;
    for (var u = 0; u < this.vertices; u++) {
        if (this.color[u] === colors.WHITE) {
            dfs_visit(u);
            console.log();
        }
    }
}

function dfs_visit(u) {
    g.color[u] = colors.GRAY;
    g.time++;
    g.d[u] = g.time;
    process.stdout.write('(' + u + ' ');
    for (var v = 0; v < g.vertices; v++) {
        if (g.adj[u][v] == 1) {
            if (g.color[v] === colors.WHITE) {
                g.pi[v] = u;
                dfs_visit(v);
            }
        }
    }
    g.color[u] = colors.BLACK;
    g.time++;
    g.f[u] = g.time;
    process.stdout.write(u + ') ');
}

function path(s, v) {
    if (s == v) {
        process.stdout.write(s);
    } else if (g.pi[v] == values.NIL) {
        process.stdout.write('No path from ' + s + ' to ' + v);
    } else {
        path(s, g.pi[v]);
        process.stdout.write(' -> ' + v);
    }
}

g = new Graph(8);
g.showGraph();
g.dfs();
g.bfs(0);
g.path(4, 7);
