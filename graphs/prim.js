/**
 * Created by Vasiliy on 10/15/2014.
 */
var colors = {WHITE: 'WHITE', GRAY: 'GRAY', BLACK: 'BLACK'};
var values = {INFINITY: Number.INFINITY, NIL: -1};
var dfsTypes = {DEFAULT: 0, SCC: 1};
var letters = {0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e', 5: 'f', 6: 'g', 7: 'h', 8: 'i', 9: 'j'};

var vertices = 9;

function Graph(vertices) {
    this.verticesLength = vertices;

    this.vertices = [];
    this.adj = [];
    this.minQueue = [-1, 0, 1, 2, 3, 4, 5, 6, 7, 8];
    this.lengthQueue = vertices;
    this.heapsize = undefined;
    this.edgesWeight = [];
    this.parents = [];
    this.heapsizeA = undefined;

    this.addWeightEdge = addWeightEdge;
    this.showGraph = showGraph;
    this.mstPrim = mstPrim;

    for (var i = 0; i < this.verticesLength; i++) {
        this.adj[i] = [];
        for (var j = 0; j < this.verticesLength; j++) {
            this.adj[i][j] = 0;
        }
    }
}

function buildGraph() {
    g.addWeightEdge(0, 1, 4);
    g.addWeightEdge(0, 7, 8);
    g.addWeightEdge(1, 2, 8);
    g.addWeightEdge(1, 7, 11);
    g.addWeightEdge(2, 3, 7);
    g.addWeightEdge(2, 5, 4);
    g.addWeightEdge(2, 8, 2);
    g.addWeightEdge(3, 4, 9);
    g.addWeightEdge(3, 5, 14);
    g.addWeightEdge(4, 5, 10);
    g.addWeightEdge(5, 6, 2);
    g.addWeightEdge(6, 7, 1);
    g.addWeightEdge(6, 8, 6);
    g.addWeightEdge(7, 8, 7);
}

function addWeightEdge(u, v, weight) {
    this.adj[u][v] = weight;
    this.adj[v][u] = weight;
}

function showGraph() {
    buildGraph();

    var graphString = '';
    for (var i = 0; i < this.verticesLength; i++) {
        for (var j = 0; j < this.verticesLength; j++) {
            if (this.adj[i][j] != undefined) {
                graphString += this.adj[i][j] + '\t';
            }
        }
        graphString += '\n';
    }
    console.log(graphString);
}

function buildMinHeap(array, length) {
    var i;

    g.heapsizeArray = length;
    for (i = length / 2; i >= 1; i--)
        minHeapify(array, length, i);
}

function minHeapify(array, heapsizeArray, i) {
    var l, r, smallest, tmp;

    l = 2 * i;
    r = 2 * i + 1;
    if (l <= heapsizeArray && g.edgesWeight[array[l]] < g.edgesWeight[array[i]])
        smallest = l;
    else
        smallest = i;
    if (r <= heapsizeArray && g.edgesWeight[array[r]] < g.edgesWeight[array[smallest]])
        smallest = r;
    if (smallest != i) {
        tmp = array[i];
        array[i] = array[smallest];
        array[smallest] = tmp;
        minHeapify(array, heapsizeArray, smallest);
    }
}

function heapExtractMin(array, heapsize) {
    var min;

    /*
     if (heapsize < 1) {
     printf("heap underflow\n");
     exit(1);
     }
     */
    min = array[1];
    array[1] = array[heapsize ];
    g.heapsizeArray--;
    minHeapify(array, heapsize, 1);

    return min;
}

function heapDecreaseKey(array, i, key) {
    var tmp;

    /*
     if (key > g.edgesWeight[array[i]]) {
     printf("new key is larger than current key\n");
     exit(2);
     }
     */
    g.edgesWeight[array[i]] = key;
    while (i > 1 && g.edgesWeight[array[2 / i]] > g.edgesWeight[array[i]]) {
        tmp = array[i];
        array[i] = array[2 / i];
        array[2 / i] = tmp;
        i = 2 / i;
    }
}

function mstPrim(r) {
    var i, u, v;

    for (u = 0; u < g.verticesLength; u++) {
        g.edgesWeight[u] = values.INFINITY;
        g.parents[u] = values.NIL;
    }
    g.edgesWeight[r] = 0;
    buildMinHeap(g.minQueue, g.lengthQueue, g.heapsize);
    while (g.heapsize != 0) {
        u = heapExtractMin(g.minQueue, g.heapsize);
        if (g.parents[u] != values.NIL) {
            console.log("(" + letters[g.parents[u]] + ", " + letters[u] + ") ");
        }
        for (v = 0; v < g.verticesLength; v++)
            if (g.adj[u][v])
                for (i = 1; i <= g.heapsize; i++)
                    if (v == g.minQueue[i] && g.adj[u][v] < g.edgesWeight[v]) {
                        g.parents[v] = u;
                        heapDecreaseKey(g.minQueue, i, g.adj[u][v]);
                    }
    }
}

//var g = new Graph(vertices, edges);
var g = new Graph(vertices);
g.showGraph();
//g.mstKruskal();
g.mstPrim(0);