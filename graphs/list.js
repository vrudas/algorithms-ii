/**
 * Created by Vasiliy on 9/4/2014.
 */
var colors = {WHITE: 'WHITE', GRAY: 'GRAY', BLACK: 'BLACK'};
var values = {INFINITY: Number.INFINITY, NIL: null};
var dfsTypes = {DEFAULT: 0, SCC: 1};
var letters = {0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e', 5: 'f', 6: 'g', 7: 'h', 8: 'i', 9: 'j'};

function Graph(v) {
    this.vertices = v;
    this.edges = 0;
    this.time = 0;
    this.setSize = 0;
    this.adj = [];
    this.adjT = [];
    this.color = [];
    this.d = [];
    this.f = [];
    this.pi = [];
    this.tlist = [];
    this.queueO = [];
    this.queueT = [];
    this.setList = [];

    this.addEdge = addEdge;
    this.addDirectedEdge = addDirectedEdge;
    this.showGraph = showGraph;
    this.bfs = bfs;
    this.path = path;
    this.dfs = dfs;
    this.topologicalSort = topologicalSort;
    this.makeAdjT = makeAdjT;

    this.makeSet = makeSet;
    this.union = union;
    this.findSet = findSet;
    this.connectedComponents = connectedComponents;
    this.sameComponents = sameComponents;
    this.printDisjointSets = printDisjointSets;

    for (var i = 0; i < this.vertices; i++) {
        this.adj[i] = [];
        this.adjT[i] = [];
        this.d[i] = values.INFINITY;
        this.color[i] = colors.WHITE;
        this.pi[i] = values.NIL;
        this.setList[i] = [];
    }
}

function Elem(key, next, prev) {
    this.key = key;
    this.next = next;
    this.prev = prev;
}

function DynamicSet(head, tail, size) {
    this.head = head;
    this.tail = tail;
    this.size = size;
}

function addEdge(u, v) {
    this.adj[u].push(v);
    this.adj[v].push(u);
    this.edges++;
}

function addDirectedEdge(u, v) {
    this.adj[u].push(v);
    this.edges++;
}

function buildGraph() {
    console.log(" Figure 21.1                                                         ");
    console.log(" a --- b     e --- f     h     j     0 --- 1     4 --- 5     7     9 ");
    console.log(" |   / |     |           |           |   / |     |           |       ");
    console.log(" |  /  |     |           |           |  /  |     |           |       ");
    console.log(" | /   |     |           |           | /   |     |           |       ");
    console.log(" c     d     g           i           2     3     6           8       \n");


    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(1, 3);
    g.addEdge(4, 5);
    g.addEdge(4, 6);
    g.addEdge(7, 8);
    g.addEdge(9, 9);

}
/**
 *  function build graph and shows its representation
 */
function showGraph() {
    buildGraph();

    var graphString = '';
    for (var i = 0; i < this.vertices; i++) {
        graphString += i + ' -> ';
        for (var j = 0; j < this.vertices; j++) {
            if (this.adj[i][j] != undefined) {
                graphString += this.adj[i][j] + ' ';
            }
        }
        graphString += '\n';
    }
    console.log(graphString);


}

function bfs(s) {
    this.color[s] = colors.GRAY;
    this.d[s] = 0;
    this.pi[s] = values.NIL;
    var queue = [];
    queue.push(s);

    while (queue.length != 0) {
        var u = queue.shift();
        for (var i = 0; i < this.adj[u].length; i++) {
            var v = this.adj[u][i];
            if (this.color[v] === colors.WHITE) {
                this.color[v] = colors.GRAY;
                this.d[v] = this.d[u] + 1;
                this.pi[v] = u;
                queue.push(v);
            }
        }
        this.color[u] = colors.BLACK;
    }
}

function dfs(type) {
    for (var u = 0; u < this.vertices; u++) {
        this.color[u] = colors.WHITE;
        this.pi[u] = values.NIL;
    }
    this.time = 0;
    switch (type) {
        case dfsTypes.DEFAULT:
            for (u = 0; u < this.vertices; u++) {
                if (this.color[u] === colors.WHITE) {
                    dfsVisit(u);
                    console.log();
                }
            }
            break;
        case dfsTypes.SCC:
            for (var i = this.vertices - 1; i >= 0; i--) {
                var k = g.queueO[i];
                if (g.color[k] === colors.WHITE) {
                    dfsVisitT(k);
                    console.log();
                }
            }
            break;
        default:
            console.error('No such type of DFS algorithm!');
            break;
    }
}

function dfsVisit(u) {
    g.color[u] = colors.GRAY;
    g.time++;
    g.d[u] = g.time;
    process.stdout.write('(' + u + ' ');
    for (var i = 0; i < g.adj[u].length; i++) {
        var v = g.adj[u][i];
        if (g.color[v] === colors.WHITE) {
            g.pi[v] = u;
            dfsVisit(v);
        }
    }
    g.color[u] = colors.BLACK;
    g.time++;
    g.f[u] = g.time;
    g.tlist.push(u);
    g.queueO.push(u);
    process.stdout.write(u + ') ');
}

function path(s, v) {
    if (s === v) {
        process.stdout.write(s);
    } else if (g.pi[v] === values.NIL) {
        process.stdout.write('No path from ' + s + ' to ' + v);
    } else {
        path(s, g.pi[v]);
        process.stdout.write(' -> ' + v);
    }
}

function topologicalSort() {
    g.dfs(dfsTypes.DEFAULT);
    console.log(g.tlist.reverse());
}

function makeAdjT() {
    for (var i = 0; i < g.vertices; i++) {
        for (var j = 0; j < g.adj[i].length; j++) {
            var elem = g.adj[i][j];
            g.adjT[elem].push(i);
        }
    }
}

function dfsVisitT(u) {
    g.color[u] = colors.GRAY;
    g.time++;
    g.d[u] = g.time;
    process.stdout.write('(' + u + ' ');
    for (var i = 0; i < g.adjT[u].length; i++) {
        var v = g.adjT[u][i];
        if (g.color[v] === colors.WHITE) {
            g.pi[v] = u;
            dfsVisitT(v);
        }
    }
    g.color[u] = colors.BLACK;
    g.time++;
    g.f[u] = g.time;
    g.tlist.push(u);
    g.queueT.push(u);
    process.stdout.write(u + ') ');
}

function makeSet(x) {
    var h = new Elem();
    h.key = x;
    h.next = null;
    h.prev = h;

    g.setList[g.setSize].head = h;
    g.setList[g.setSize].tail = h;
    g.setList[g.setSize].size = 1;
    g.setSize++;

    g.tlist[x] = h;
}

function union(x, y) {
    var xp;
    var i, xi, yi;
    for (i = 0; i < g.setSize; i++) {
        if (g.setList[i].head === g.tlist[x].prev) {
            xi = i;
        } else if (g.setList[i].head === g.tlist[y].prev) {
            yi = i;
        }
    }

    if (g.setList[yi].size < g.setList[xi].size) {
        i = xi;
        xi = yi;
        yi = i;
    }

    xp = g.setList[xi].head;
    while (xp != null) {
        xp.prev = g.setList[yi].head;
        xp = xp.next;
    }

    g.setList[yi].tail.next = g.setList[xi].head;
    g.setList[yi].tail = g.setList[xi].tail;
    g.setList[yi].size += g.setList[xi].size;

    g.setList[xi].head = null;
    g.setList[xi].tail = null;
    g.setList[xi].size = 0;
}

function findSet(v) {
    return g.tlist[v].prev;
}

function connectedComponents() {
    var u, v, k;
    for (v = 0; v < g.vertices; v++) {
        g.makeSet(v);
    }
    for (u = 0; u < g.vertices; u++) {
        for (k = 0; k < g.adj[u].length; k++) {
            v = g.adj[u][k];
            if (findSet(u) !== findSet(v)) {
                g.union(u, v);
            }
        }
    }
}

function sameComponents(u, v) {
    return g.findSet(u) === g.findSet(v);
}

function printDisjointSets() {
    var i, h;

    for (i = 0; i < g.setSize; i++) {
        h = g.setList[i].head;
        if (h !== null) {
            process.stdout.write('S[' + i + ']');
            while (h != null) {
                process.stdout.write(' -> ' + letters[h.key]);
                h = h.next;
            }
            console.log();
        }
    }
}

g = new Graph(10);
g.showGraph();
g.connectedComponents();
g.printDisjointSets();

