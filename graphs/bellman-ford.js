var colors = {WHITE: 'WHITE', GRAY: 'GRAY', BLACK: 'BLACK'};
var values = {INFINITY: Number.INFINITY, NIL: -1};
var dfsTypes = {DEFAULT: 0, SCC: 1};
var letters = {0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e', 5: 'f', 6: 'g', 7: 'h', 8: 'i', 9: 'j'};

var vertices = 5;

function Graph(vertices) {
    this.verticesLength = vertices;

    this.vertices = [];
    this.adj = [];
    this.d = [];
    this.pi = [];

    for (var i = 0; i < this.verticesLength; i++) {
        this.adj[i] = [];
    }

    this.buildGraph = buildGraph;
    this.addWeightEdge = addWeightEdge;
    this.printGraph = printGraph;
    this.initSingleSource = initSingleSource;
    this.relax = relax;
    this.bellamFord = bellmanFord;
    this.path = path;
}

function Elem(v, w, next) {
    this.v = v;
    this.w = w;
    this.next = next;
}

function addWeightEdge(u, v, weight) {
    g.adj[u] = new Elem(v, weight, g.adj[u]);
}

function path(s, v) {
    if (s == v) {
        process.stdout.write('' + s);
    } else if (g.pi[v] == values.NIL) {
        process.stdout.write('No path from ' + s + ' to ' + v);
    } else {
        path(s, g.pi[v]);
        process.stdout.write(' -> ' + v);
    }
}

function buildGraph() {
    //  Figure 24.4
    g.addWeightEdge(0, 1, 6);         //        _-2__           _-2__
    g.addWeightEdge(0, 3, 7);         //       /     \         /     \
    g.addWeightEdge(1, 2, 5);         //      t---5-->x       1---5-->2
    g.addWeightEdge(1, 3, 8);         //     /|\     /^      /|\     /^
    g.addWeightEdge(1, 4, -4);        //    6 | \   / |     6 | \   / |
    g.addWeightEdge(2, 1, -2);        //   /  |  \ /  |    /  |  \ /  |
    g.addWeightEdge(3, 2, -3);        //  s   8   /   7   0   8   /   7
    g.addWeightEdge(3, 4, 9);         //   \  | -3 \  |    \  | -3 \  |
    g.addWeightEdge(4, 0, 2);         //    7 | /  -4 |     7 | /  -4 |
    g.addWeightEdge(4, 2, 7);         //     \v/     \|      \v/     \|
                                      //      y---9-->z       3---9-->4
    g.printGraph();
}

function printGraph() {
    for (var i = 0; i < vertices; i++) {
        process.stdout.write('Adj[' + i + ']= ' + i);
        for (var head = g.adj[i]; head != null; head = head.next) {
            if (head.w || head.v) {
                process.stdout.write(' --w' + head.w + '--> ' + head.v);
            }
        }
        console.log();
    }
}

/**
 * BELLMAN-FORD ALGORITHM
 */
function relax(u, v, w) {
    if (g.d[v] > g.d[u] + w) {
        g.d[v] = g.d[u] + w;
        g.pi[v] = u;
    }
}

function initSingleSource(n, s) {
    for (var v = 0; v < n; v++) {
        g.d[v] = 32762;
        g.pi[v] = -1;
    }
    g.d[s] = 0;
}

function bellmanFord(n, s) {
    var u, v, head;

    g.initSingleSource(n, s);
    for (v = 1; v < n; v++) {
        for (u = 0; u < n; u++) {
            for (head = g.adj[u]; head != null; head = head.next) {
                if (head.w || head.v) {
                    g.relax(u, head.v, head.w);
                }
            }
        }
    }

    for (u = 0; u < n; u++) {
        for (head = g.adj[u]; head != null; head = head.next) {
            if (g.d[head.v] > g.d[u] + head.w) {
                return false;
            }
        }
    }

    return true;
}

var g = new Graph(vertices);
g.buildGraph();
g.bellamFord(vertices, 0);
g.path(0, 4);

/**
 * END BELLMAN-FORD ALGORITHM
 */


