var colors = {WHITE: 'WHITE', GRAY: 'GRAY', BLACK: 'BLACK'};
var values = {INFINITY: 32000, NIL: -32000};
var dfsTypes = {DEFAULT: 0, SCC: 1};
var letters = {0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e', 5: 'f', 6: 'g', 7: 'h', 8: 'i', 9: 'j'};

var vertices = 6;

function Graph(vertices) {
    this.vertices = vertices;
    this.time = 0;

    this.adj = [];
    this.d = [];
    this.f = [];
    this.pi = [];
    this.tlist = [];
    this.queueO = [];
    this.dfsD = [];
    this.dfsPi = [];
    this.color = [];

    for (var i = 0; i < this.vertices; i++) {
        this.adj[i] = [];
    }

    this.buildGraph = buildGraph;
    this.addWeightEdge = addWeightEdge;
    this.printGraph = printGraph;
    this.initSingleSource = initSingleSource;
    this.relax = relax;
    this.dagAlgorithm = dagAlgorithm;
    this.path = path;
    this.dfs = dfs;
    this.dfsVisit = dfsVisit;
    this.topologicalSort = topologicalSort;
}

function Elem(v, w, next) {
    this.v = v;
    this.w = w;
    this.next = next;
}

function addWeightEdge(u, v, weight) {
    g.adj[u] = new Elem(v, weight, g.adj[u]);
}

function path(s, v) {
    if (s == v) {
        process.stdout.write('' + s);
    } else if (g.pi[v] == values.NIL) {
        process.stdout.write('No path from ' + s + ' to ' + v);
    } else {
        path(s, g.pi[v]);
        process.stdout.write(' -> ' + v);
    }
}

function buildGraph() {
    addWeightEdge(0, 2, 5);
    addWeightEdge(0, 1, 3);
    addWeightEdge(2, 1, 2);
    addWeightEdge(2, 3, 6);
    addWeightEdge(1, 3, 7);
    addWeightEdge(1, 4, 4);
    addWeightEdge(1, 5, 2);
    addWeightEdge(3, 4, -1);
    addWeightEdge(3, 5, 1);
    addWeightEdge(4, 5, -2);
    g.printGraph();
}

function printGraph() {
    for (var i = 0; i < vertices; i++) {
        process.stdout.write('Adj[' + i + ']= ' + i);
        for (var head = g.adj[i]; head != null; head = head.next) {
            if (head.w || head.v) {
                process.stdout.write(' --w' + head.w + '--> ' + head.v);
            }
        }
        console.log();
    }
}

/**
 * DAG ALGORITHM
 */
function relax(u, v, w) {
    if (g.d[v] > g.d[u] + w) {
        g.d[v] = g.d[u] + w;
        g.pi[v] = u;
    }
}

function initSingleSource(n, s) {
    for (var v = 0; v < n; v++) {
        g.d[v] = values.INFINITY;
        g.pi[v] = values.NIL;
    }
    g.d[s] = 0;
}

function dagAlgorithm(n, s) {
    var i, u, v;

    g.topologicalSort();
    g.initSingleSource(n, s);

    for (i = n - 1; i >= 0; i--) {
        u = g.tlist[i];
        for (v = g.adj[u]; v != null; v = v.next) {
            g.relax(u, v.v, v.w)
        }
    }
}

function dfs() {
    for (var u = 0; u < g.vertices; u++) {
        g.color[u] = colors.WHITE;
        g.dfsPi[u] = values.NIL;
    }
    g.time = 0;

    for (u = 0; u < g.vertices; u++) {
        if (g.color[u] === colors.WHITE) {
            g.dfsVisit(u);
            console.log();
        }
    }
}

function dfsVisit(u) {
    g.color[u] = colors.GRAY;
    g.time++;
    g.dfsD[u] = g.time;
    process.stdout.write('(' + u + ' ');
    for (var v = g.adj[u]; v != null; v = v.next) {
        if (g.color[v.v] === colors.WHITE) {
            g.dfsPi[v.v] = u;
            dfsVisit(v.v);
        }
    }
    g.color[u] = colors.BLACK;
    g.time++;
    g.f[u] = g.time;
    g.tlist.push(u);
    g.queueO.push(u);
    process.stdout.write(u + ') ');
}

function topologicalSort() {
    g.dfs();
    //console.log(g.tlist.reverse());
}

var g = new Graph(vertices);
g.buildGraph();
g.dagAlgorithm(vertices, 2);
for (var i = 0; i < vertices; i++)
    if (g.pi[i] > values.NIL)
        console.log('(' + g.pi[i] + '\t' + i + ')');
/**
 * END DAG ALGORITHM
 */


