/**
 * Created by Vasiliy on 10/14/2014.
 */
var colors = {WHITE: 'WHITE', GRAY: 'GRAY', BLACK: 'BLACK'};
var values = {INFINITY: Number.INFINITY, NIL: null};
var dfsTypes = {DEFAULT: 0, SCC: 1};
var letters = {0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e', 5: 'f', 6: 'g', 7: 'h', 8: 'i', 9: 'j'};

var vertices = 9;
var edges = 14;

function Graph(vertices, edges) {
    this.verticesLength = vertices;
    this.edgesLength = edges;

    this.edges = [];
    this.vertices = [];
    this.adj = [];
    this.edgeSize = 0;
    this.setSize = 0;
    this.setList = [];


    this.addWeightEdge = addWeightEdge;
    this.showGraph = showGraph;
    this.mstKruskal = mstKruskal;

    for (var i = 0; i < this.verticesLength; i++) {
        this.adj[i] = [];
        for (var j = 0; j < this.verticesLength; j++) {
            this.adj[i][j] = 0;
        }
    }
    for (var k = 0; k < this.edgesLength; k++) {
        this.setList[k] = [];
    }
}

function Edge(u, v, weight) {
    this.u = u;
    this.v = v;
    this.weight = weight;
}

function Elem(key, next, prev) {
    this.key = key;
    this.next = next;
    this.prev = prev;
}


function buildGraph() {
    g.addWeightEdge(0, 1, 4);
    g.addWeightEdge(0, 7, 8);
    g.addWeightEdge(1, 2, 8);
    g.addWeightEdge(1, 7, 11);
    g.addWeightEdge(2, 3, 7);
    g.addWeightEdge(2, 5, 4);
    g.addWeightEdge(2, 8, 2);
    g.addWeightEdge(3, 4, 9);
    g.addWeightEdge(3, 5, 14);
    g.addWeightEdge(4, 5, 10);
    g.addWeightEdge(5, 6, 2);
    g.addWeightEdge(6, 7, 1);
    g.addWeightEdge(6, 8, 6);
    g.addWeightEdge(7, 8, 7);
}

function addWeightEdge(u, v, weight) {
    this.adj[u][v] = weight;
    this.adj[v][u] = weight;

    g.edges[g.edgeSize] = new Edge(u, v, weight);
    g.edgeSize++;
}

function showGraph() {
    buildGraph();

    var graphString = '';
    for (var i = 0; i < this.verticesLength; i++) {
        for (var j = 0; j < this.verticesLength; j++) {
            if (this.adj[i][j] != undefined) {
                graphString += this.adj[i][j] + '\t';
            }
        }
        graphString += '\n';
    }
    console.log(graphString);
}

function makeSet(x) {
    var h = new Elem();
    h.key = x;
    h.next = null;
    h.prev = h;

    g.setList[g.setSize].head = h;
    g.setList[g.setSize].tail = h;
    g.setList[g.setSize].size = 1;
    g.setSize++;

    g.vertices[x] = h;
}

function union(x, y) {
    var xp;
    var i, xi, yi;
    for (i = 0; i < g.setSize; i++) {
        if (g.setList[i].head === g.vertices[x].prev) {
            xi = i;
        } else if (g.setList[i].head === g.vertices[y].prev) {
            yi = i;
        }
    }

    if (g.setList[yi].size < g.setList[xi].size) {
        i = xi;
        xi = yi;
        yi = i;
    }

    xp = g.setList[xi].head;
    while (xp != null) {
        xp.prev = g.setList[yi].head;
        xp = xp.next;
    }

    g.setList[yi].tail.next = g.setList[xi].head;
    g.setList[yi].tail = g.setList[xi].tail;
    g.setList[yi].size += g.setList[xi].size;

    g.setList[xi].head = null;
    g.setList[xi].tail = null;
    g.setList[xi].size = 0;
}

function findSet(v) {
    return g.vertices[v].prev;
}

function partition(edges, p, r) {
    var x, i, j;

    x = edges[r].weight;
    i = p - 1;
    for (j = p; j <= r - 1; j++)
        if (edges[j].weight <= x) {
            i++;
            swap(edges, i, j);
        }
    swap(edges, i + 1, r);

    return i + 1;
}

function swap(array, i, j) {
    var tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
}

function quickSort(edges, p, r) {
    var q;

    if (p < r) {
        q = partition(edges, p, r);
        quickSort(edges, p, q - 1);
        quickSort(edges, q + 1, r);
    }
}

function mstKruskal() {
    var i, v;

    for (v = 0; v < g.verticesLength; v++) {
        makeSet(v)
    }
    quickSort(g.edges, 0, g.edgesLength - 1);
    for (i = 0; i < g.edgesLength; i++)
        if (findSet(g.edges[i].u) != findSet(g.edges[i].v)) {
            union(g.edges[i].u, g.edges[i].v);
            process.stdout.write("(" + letters[g.edges[i].u] + ", " + letters[g.edges[i].v] + ") ");
        }
}

var g = new Graph(vertices, edges);
g.showGraph();
g.mstKruskal();